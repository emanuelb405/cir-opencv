import os, glob, argparse
import cv2
from scipy.io import loadmat
from collections import defaultdict
import numpy as np
from lxml import etree, objectify
import re
from tqdm import tqdm, trange


def vbb_anno2dict(vbb_file, person_types=None):
    """
    Parse caltech vbb annotation file to dict
    Args:
        vbb_file: input vbb file path
        person_types: list of person type that will be used (total 4 types: person, person-fa, person?, people).
            If None, all will be used:
    Return:
        Annotation info dict with filename as key and anno info as value
    """
    filename = os.path.splitext(os.path.basename(vbb_file))[0]
    annos = defaultdict(dict)
    vbb = loadmat(vbb_file)
    # object info in each frame: id, pos, occlusion, lock, posv
    objLists = vbb['A'][0][0][1][0]
    objLbl = [str(v[0]) for v in vbb['A'][0][0][4][0]]
    # person index
    if not person_types:
        person_types = ["person", "person-fa", "person?", "people", "pedestrian", "ped"]
    person_index_list = [x for x in range(len(objLbl)) if re.sub("\d","",objLbl[x]) in person_types]
    for frame_id, obj in enumerate(objLists):
        if len(obj) > 0:
            frame_name = str(filename) + "_" + str(frame_id+1) + ".jpg"
            annos[frame_name] = defaultdict(list)
            annos[frame_name]["id"] = frame_name
            if obj.shape[1] > 0:
                for fid, pos, occl in zip(obj['id'][0], obj['pos'][0], obj['occl'][0]):
                    fid = int(fid[0][0]) - 1  # for matlab start from 1 not 0
                    if not fid in person_index_list:  # only use bbox whose label is given person type
                        continue
                    annos[frame_name]["label"].append('person')
                    pos = pos[0].tolist()
                    occl = int(occl[0][0])
                    annos[frame_name]["occlusion"].append(occl)
                    annos[frame_name]["bbox"].append(pos)
                if not annos[frame_name]["bbox"]:
                    del annos[frame_name]
            else:
                del annos[frame_name]
    return annos


def vid2img(annos, vid_file, outdir):
    """
    Extract frames in vid files to given output directories
    Args:
         annos: annos dict returned from parsed vbb file
         vid_file: vid file path
         outdir: frame save dir
    Returns:
        camera captured image size
    """
    cap = cv2.VideoCapture(vid_file)
    index = 1
    # captured frame list
    v_id = os.path.splitext(os.path.basename(vid_file))[0]
    cap_frames_index = np.sort([int(os.path.splitext(id)[0].split("_")[2]) for id in annos.keys()])
    while True:
        ret, frame = cap.read()
        if ret:
            if not index in cap_frames_index:
                index += 1
                continue
            if not os.path.exists(outdir):
                os.makedirs(outdir)
            outname = os.path.join(outdir, v_id+"_"+str(index)+".jpg")
            #print("Current frame: ", v_id, str(index))
            cv2.imwrite(outname, frame)
            height, width, _ = frame.shape
        else:
            break
        index += 1
    cap.release()
    img_size = (width, height)
    return img_size


def instance2xml_base(anno, folder, img_size, dictdat, bbox_type='xyxy'):
    """
    Parse annotation data to VOC XML format
    Args:
        anno: annotation info returned by vbb_anno2dict function
        img_size: camera captured image size
        bbox_type: bbox coordinate record format: xyxy (xmin, ymin, xmax, ymax); xywh (xmin, ymin, width, height)
    Returns:
        Annotation xml info tree
    """
    assert bbox_type in ['xyxy', 'xywh']
    E = objectify.ElementMaker(annotate=False)
    anno_tree = E.annotation(
        E.folder(folder),
        E.filename(anno['id']),
        E.source(
            E.database(dictdat['database']),
            E.annotation(dictdat['annotation']),
            E.image(dictdat['image']),
            E.url(dictdat['url'])
        ),
        E.size(
            E.width(img_size[0]),
            E.height(img_size[1]),
            E.depth(3)
        ),
        E.segmented(0),
    )
    for index, bbox in enumerate(anno['bbox']):
        bbox = [float(x) for x in bbox]
        if bbox_type == 'xyxy':
            xmin, ymin, w, h = bbox
            xmax = xmin+w
            ymax = ymin+h
        else:
            xmin, ymin, xmax, ymax = bbox
        xmin = int(xmin)
        ymin = int(ymin)
        xmax = int(xmax)
        ymax = int(ymax)
        if xmin < 0:
            xmin = 0
        if xmax > img_size[0] - 1:
            xmax = img_size[0] - 1
        if ymin < 0:
            ymin = 0
        if ymax > img_size[1] - 1:
            ymax = img_size[1] - 1
        if ymax <= ymin or xmax <= xmin:
            continue
        E = objectify.ElementMaker(annotate=False)
        anno_tree.append(
            E.object(
            E.name(anno['label'][index]),
            E.bndbox(
                E.xmin(xmin),
                E.ymin(ymin),
                E.xmax(xmax),
                E.ymax(ymax)
            ),
            E.difficult(0),
            E.occlusion(anno["occlusion"][index])
            )
        )
    return anno_tree


def parse_anno_file(vbb_inputdir, vid_inputdir, vbb_outputdir, vid_outputdir, person_types=None, dictdat=None):
    """
    Parse Caltech data stored in vid and vbb files to VOC xml format
    Args:
        vbb_inputdir: vbb file saved pth
        vid_inputdir: vid file saved path
        vbb_outputdir: vbb data converted xml file saved path
        vid_outputdir: vid data converted frame image file saved path
        person_types: list of person type that will be used (total 4 types: person, person-fa, person?, people).
            If None, all will be used.
        dictdat: dict containing information about the used dataset.
            Keys: 'database', 'annotation', 'image', 'url'
    """
    # annotation sub-directories in hda annotation input directory
    assert os.path.exists(vbb_inputdir)
    
    if dictdat == None:  
        dictdat = {'database': '', 'annotation': '', 'image': '', 'url': ''}
    
    vbb_files = glob.glob(os.path.join(vbb_inputdir, "*.vbb"))
    vbb_files.sort(key = lambda x: int(os.path.splitext(os.path.split(x)[1])[0][-4:]))
    for vbb_file in tqdm(vbb_files, desc='vbb file'):
        # extract annotations from vbb
        #print("Parsing annotations of file: ", vbb_file)
        annos = vbb_anno2dict(vbb_file, person_types=person_types)
        if annos:
            vbb_outdir = vbb_outputdir
            # extract frames from video
            vid_file = os.path.join(vid_inputdir, os.path.splitext(os.path.basename(vbb_file))[0]+".mp4")
            vid_outdir = vid_outputdir
            if not os.path.exists(vbb_outdir):
                os.makedirs(vbb_outdir)
            if not os.path.exists(vid_outdir):
                os.makedirs(vid_outdir)
            img_size = vid2img(annos, vid_file, vid_outdir)
            for filename, anno in tqdm(sorted(annos.items(), key=lambda x: x[0]), desc='annotation'):
                if "bbox" in anno:
                    anno_tree = instance2xml_base(anno, vid_outdir, img_size, dictdat, )
                    outfile = os.path.join(vbb_outdir, os.path.splitext(filename)[0]+".xml")
                    #print("Generating annotation xml file of picture: ", filename)
                    etree.ElementTree(anno_tree).write(outfile, pretty_print=True)


def visualize_bbox(xml_file, img_file):
    import cv2
    tree = etree.parse(xml_file)
    # load image
    image = cv2.imread(img_file)
    # get bbox
    for bbox in tree.xpath('//bndbox'):
        coord = []
        for corner in bbox.getchildren():
            coord.append(int(float(corner.text)))
        # draw rectangle
        # coord = [int(x) for x in coord]
        image = cv2.rectangle(image, (coord[0], coord[1]), (coord[2], coord[3]), (0, 0, 255), 2)
    # visualize image
    cv2.imshow("test", image)
    cv2.waitKey(0)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-vid_dir", help="Dataset video data root directory")
    parser.add_argument("-vbb_dir", help="Dataset vbb data root directory")
    parser.add_argument("-output_dir", help="Root saving path for frame and annotation files")
    parser.add_argument("-person_type", default="person", 
										nargs="+",
										type=str, help="Person type extracted within 6 options: "
														"'person', 'person-fa', 'person?', 'people', 'pedestrian', 'ped'. If multiple type used,"
														"separated with comma",
										choices=["person", "person-fa", "person?", "people", "pedestrian", "ped"])
    args = parser.parse_args()
    outdir = args.output_dir
    frame_out = os.path.join(outdir, "frame")
    anno_out = os.path.join(outdir, "annotation")
    person_type = args.person_type
    parse_anno_file(args.vid_dir, args.vbb_dir, frame_out, anno_out, person_type)
    print("Generating done!")


def test():
    vid_inputdir = "sequence\\set1"
    vbb_inputdir = "annotation\\set1"
    vid_outputdir = "extracted\\frame\\set1"
    vbb_outputdir = "extracted\\annot\\set1"
    person_types = ["pedestrian", "ped"]
    dataset = {'database': 'JAAD', 'annotation': 'JAAD_pedestrian', 'image': 'JAAD', 'url': 'http://data.nvision2.eecs.yorku.ca/JAAD_dataset/'}
    parse_anno_file(vbb_inputdir, vid_inputdir, vbb_outputdir, vid_outputdir, person_types=person_types, dictdat=dataset)
#    xml_file = "extracted\\annot\\video_0001_1.xml"
#    img_file = "extracted\\frame\\video_0001_1.jpg"
#    visualize_bbox(xml_file, img_file)


if __name__ == "__main__":
    test()
#    main()
